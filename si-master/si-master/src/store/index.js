import Vue from 'vue'
import Vuex from 'vuex'
import {get_account_info} from '@/api/api'
Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        account_id:'',
        risk_res:'',
        total_assets:'',
        recommendStep:0,
        recommendCurStep:0,
        fund:[],
        start_date:'2016-2-1',
        end_date:'2020-8-27',
        key:1,
        option:1
    },
    mutations:{
        setFund (state,fund) {
            state.fund = fund
        },
        setAccount_id (state, id) {
            state.account_id = id
        },
        setRecommendStep (state, N) {
            state.recommendStep = N
        },
        setRecommendCurStep (state, N) {
            if (state.recommendStep >= N) {
                state.recommendCurStep = N
            } else {
                console.log('请完成当前步骤')
            }
        },
        setRisk_res (state, res) {
            state.risk_res = res
        },
        setTotal_assets (state, total) {
            state.total_assets = total
        },
        setOption(state,option){
            state.option = option
        },
        setKey(state,key){
            state.key = key
        },
        setDate (state, change) {
            if (change.type == 'start') {
                console.log(1,change.date)
                 state.start_date = change.date
            } else if (change.type == 'end') {
                state.end_date = change.date
            }
        },
        


    },
    actions:{
        get_account_info(contex) {
            return  new Promise((resolve) => {
                let data = {
                    "account_id":contex.state.account_id
                }
                get_account_info(data)
                .then(res => {
                    if (res.code == 0) {
                        contex.commit('setRisk_res', res.data.risk_res)
                        contex.commit('setTotal_assets', res.data.total_assets)
                        if ([undefined, ''].includes(contex.state.risk_res)) {
                            contex.commit('setRecommendStep', 0)
                            contex.commit('setRecommendCurStep', 0)
                        } else {
                            if ([undefined, '', 0, '0'].includes(contex.state.total_assets)) {
    
                                contex.commit('setRecommendStep', 1)
                                contex.commit('setRecommendCurStep', 1)
                            } else {
                                contex.commit('setRecommendStep', 2)
                                contex.commit('setRecommendCurStep', 2)
                            }
                        }
                        resolve(contex.state.recommendCurStep)
                        
                    }
                })
            })
            
        }
    },
    getters: {
        fundCodeList(state) {
            return state.fund.map(item => item.main_code)
        },
        fundRateList(state) {
            return state.fund.map(item => item.rate/100)
        },
        getDate: (state) => (type) => {
            if (type == 'start') {

                let tempDate = state.start_date.split('-')
                tempDate[1]--
                return new Date (...tempDate)
            } else if (type == 'end') {
                let tempDate = state.end_date.split('-')
                tempDate[1]--
                return new Date (...tempDate)
            }
        }
    }
})