// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
// import VueRouter from 'vue-router';
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import less from 'less'
import animate from 'animate.css'
import axios from 'axios'
import  * as echarts from 'echarts'
//需要挂载到Vue原型上
Vue.prototype.$echarts = echarts

Vue.use(animate)
Vue.use(less)
Vue.prototype.$axios = axios;
Vue.config.productionTip = false
Vue.use(ViewUI);

// let protocol = window.location.protocol
// let host = window.location.host
// let reg = /^localhost+/
// if (reg.test(host)) {
//   axios.defaults.baseURL = 'http:40.108.25.113:8080/testcrt'
// } else {
//   axios.defaults.baseURL = protocol + '//' + host
// }


/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
