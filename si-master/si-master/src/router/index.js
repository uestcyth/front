import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/view/main/main'
import store from '../store'

Vue.use(Router)
let map = {
    '0': '/risk',
    '1': '/amount',
    '2': '/recommend',
    '3': '/backtest'
}

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      alias:'/register',
      component: () => import('@/components/view/user/login/login'),
      // beforeEnter(to, from ,next) {
      //   if (to.path == '/login') {
      //     console.log("登录页面")
      //   } else {
      //     console.log("注册页面")
      //   }
      //   next()
      // }
    },
    // {
    //   path: '/register',
    //   name: 'register',
    //   redirect:'/login',
    //   alias:'/register'
    //   // component: () => import('@/components/view/user/register/register'),
    // },
    {
      path: '/',
      name: 'main',
      component: main,
      beforeEnter(to, from, next) {
        console.log(window.localStorage)
        if (window.localStorage.account_id) {
          console.log("已登录")
          next()
        } else {
          console.log("登录")
          next('/login')
        }
      },
      children: [
        {
          path: '/recommendate',
          name:'recommendate',
          alias:'/',
          component: () => import ('@/components/view/recommendate/recommendate'),
          children:[
            {
              path:'/risk',
              name:'risk',
              component:() => import('@/components/view/recommendate/components/questions')
            },
            {
              path:'/amount',
              name:'amount',
              component:() => import('@/components/view/recommendate/components/amount'),
              beforeEnter(to, from, next) {
               if (store.state.recommendStep >= 1) {
                  next()
               } else {
                  next(map[store.state.recommendCurStep])
               }
              },
            },
            {
              path:'/recommend',
              name:'recommend',
              component:() => import('@/components/view/recommendate/components/recommend'),
              beforeEnter(to, from, next) {
                if (store.state.recommendStep >= 2) {
                   next()
                } else {
                   next(map[store.state.recommendCurStep])
                }
               },
            },
            {
              path:'/backtest',
              name:'backtest',
              component:() => import('@/components/view/recommendate/components/backtest'),
              beforeEnter(to, from, next) {
                if (store.state.recommendStep >= 2) {
                  next()
               } else {
                  next(map[store.state.recommendCurStep])
               }
              }
            }
          ]
        },
        {
          path: '/fund',
          name:'fund',
          component: () => import ('@/components/view/fund/fund')
        },
        {
          path: '/combo',
          name:'combo',
          component: () => import ('@/components/view/combo/combo')
        },
        {
          path: '/backtest',
          name:'backtest',
          component: () => import ('@/components/view/backtest/backtest')
        },
        {
          path: '/myFavorate',
          name:'myFavorate',
          component: () => import ('@/components/view/myFavorate/myFavorate')
        },
        {
          path: '/myCombo',
          name:'myCombo',
          component: () => import ('@/components/view/myCombo/myCombo')
        },

      ]
    }
  ]
})
