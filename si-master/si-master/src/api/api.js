import {getData} from './requst'


export const test = (data) => getData('/fund/get_fund_list/',data)

export const register = (data) => getData('/account/register/',data)

export const login = (data) => getData('/account/login/',data)

export const assets_update = (data) => getData('/account/assets_update/',data)

export const get_account_info = (data) => getData('/account/get_account_info/',data)

export const testing_configure = (data) => getData('/fund/testing_configure/',data)

export const get_risk_eval = (data) => getData('/account/get_risk_eval/', data)

export const risk_eval = (data) => getData('/account/risk_eval/', data)

export const do_backtest_annualized = (data) => getData('/fund/do_backtest_annualized/', data)

export const backtestFund = (data) => getData('/backtest/backtestFund/', data)







