import axios from 'axios'

const base = '/api'
// const base = 'http://47.108.25.113:8080/'

export const getData = function (url, data) {

    return new Promise( (resolve, reject) => {
        axios({
            method:'post',
            url:base + url,
            data:data,
              headers: {
                    'Content-Type': 'application/json',
                    'Cache-Control': 'no-cache',
                    'method': 'POST',
              }
        }).then(function(res){
            console.log(res)
            if (res.status == 200) {
                resolve(res.data) 
            } else {
                reject(res.status)
            }
            // return res;
        }).catch(err => {
            console.log(err)
            reject(err)

        });
    } )
    
    // return axios.get(base + url,data)
}
